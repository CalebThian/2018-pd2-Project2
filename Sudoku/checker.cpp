#include "checker.h"

Checker::Checker()
{

}

bool Checker::checkUnity(int arr[])
{
    int arr_unity[9];

    for(int ci=0;ci<9;++ci)
    {
        arr_unity[ci]=0;//initialize arr_unity to {0}
    }
    for(int ci=0;ci<9;++ci)
    {
        if(arr[ci]>0)
            ++arr_unity[arr[ci]-1];//if arr[i] have number 1~9, such as 8, then arr_unity[7] will become 1 from 0
    }
    for(int ci=0;ci<9;++ci)
    {
        if(arr_unity[ci]>1)//not 1~9 is inside arr[]
        {
            return false;
        }
    }
     return true;
}

bool Checker::checkUnityAbs(int arr[])
{
    int arr_unity[9];

    for(int ci=0;ci<9;++ci)
    {
        arr_unity[ci]=0;//initialize arr_unity to {0}
    }
    for(int ci=0;ci<9;++ci)
    {
        if(arr[ci]>0)
            ++arr_unity[arr[ci]-1];//if arr[i] have number 1~9, such as 8, then arr_unity[7] will become 1 from 0
    }
    for(int ci=0;ci<9;++ci)
    {
        if(arr_unity[ci]!=1)//not 1~9 is inside arr[]
        {
            return false;
        }
    }
     return true;
}

bool Checker::isCorrect(int arr2[81])
{
    bool check_result;
    int check_arr[9];
    int location;

    //check row
    for(int ci=0;ci<81;ci+=9)
    {
        for(int cj=0;cj<9;++cj)
        {
            check_arr[cj] = arr2[ci+cj];
        }
        check_result = checkUnity(check_arr);
        if(check_result==false)
        {
            return false;
        }
    }

    //check columns
    for(int ci=0;ci<9;++ci)
    {
        for(int cj=0;cj<9;++cj)
        {
            check_arr[cj]=arr2[ci+cj*9];//such as 0,9,18,27......
        }
        check_result = checkUnity(check_arr);
        if(check_result==false)
        {
            return false;
        }
    }

    //check cells
    for(int ci=0;ci<9;++ci)
    {
        for(int cj=0;cj<9;++cj)
        {
            location = 27*(ci/3)+3*(ci%3)+9*(cj/3)+(cj%3);
            check_arr[cj]=arr2[location];
        }
        check_result=checkUnity(check_arr);
        if(check_result==false)
        {
            return false;
        }
    }
    return true;
}

bool Checker::isCorrectAbs(int arr2[81])
{
    bool check_result;
    int check_arr[9];
    int location;

    //check row
    for(int ci=0;ci<81;ci+=9)
    {
        for(int cj=0;cj<9;++cj)
        {
            check_arr[cj] = arr2[ci+cj];
        }
        check_result = checkUnityAbs(check_arr);
        if(check_result==false)
        {
            return false;
        }
    }

    //check columns
    for(int ci=0;ci<9;++ci)
    {
        for(int cj=0;cj<9;++cj)
        {
            check_arr[cj]=arr2[ci+cj*9];//such as 0,9,18,27......
        }
        check_result = checkUnityAbs(check_arr);
        if(check_result==false)
        {
            return false;
        }
    }

    //check cells
    for(int ci=0;ci<9;++ci)
    {
        for(int cj=0;cj<9;++cj)
        {
            location = 27*(ci/3)+3*(ci%3)+9*(cj/3)+(cj%3);
            check_arr[cj]=arr2[location];
        }
        check_result=checkUnityAbs(check_arr);
        if(check_result==false)
        {
            return false;
        }
    }
    return true;
}
