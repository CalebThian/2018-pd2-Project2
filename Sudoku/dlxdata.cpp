#include "dlxdata.h"

DLXdata::DLXdata()
{
    this->centerNo=0;
    this->colsNo=0;
    this->downNo=0;
    this->leftNo=0;
    this->rightNo=0;
    this->rowsNo=0;
    this->upNo=0;
}

DLXdata::DLXdata(int a)
{
    this->centerNo=a;
    this->colsNo=a;
    this->downNo=a;
    this->leftNo=a;
    this->rightNo=a;
    this->rowsNo=a;
    this->upNo=a;
}

int DLXdata::center()
{
    return centerNo;
}

int DLXdata::right()
{
    return rightNo;
}

int DLXdata::left()
{
    return leftNo;
}

int DLXdata::up()
{
    return upNo;
}

int DLXdata::down()
{
    return downNo;
}

int DLXdata::rows()
{
    return rowsNo;
}

int DLXdata::cols()
{
    return colsNo;
}
