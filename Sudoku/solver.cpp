#include "solver.h"

Solver::Solver()
{
    //bool vanscom[324]={0};
    std::vector <bool> vanscom(324,0);
    //bool cvanscom[324]={0};
    int qans=0;
    int ndis=0;
    int dis=0;
    int count=0;
    int ans[81]={0};
}

/*
void Solver::logger()
{
    for(int i=0;i<vansset.size();++i)
    {
        for(int j=0;j<noCol;++j)
        {
            if(vansset.at(i).at(j)==1)
            {

            }
        }
    }
}
*/

bool Solver::leter()//main of Solver
{
    while(1)
    {
        if(discriminant()==2)
        {
            return false;
        }
        else if(discriminant()==1)
        {
            discriminant();
            elementary();//this is elementary, my friend
            if(ndis==0&&dis==80)//check whether there is no more square can be filled after a period , if yes, break
            {
                dis=0;
                return 0;
            }
        ndis=0;//initialize ndis, to accumulate again how many square is filled, but ndis will just oscillates between 0 and 1
        }
    }
     tryrep();
     if(tryrep()==1)
     {
         return true;
     }
     else if(tryrep()==2)
     {
         return false;
     }

}

void Solver::producer()
{
    for(int i=0;i<81;++i)//for all empty square, find possibly number
    {
        while(Sivial.getvtotal().at(i)==0)//find possibly number
        {
            ++count;//accumulate how many squares left
            for(int j=1;j<10;++j)
            {
                //if(Sivial.getvtotal().at(((i/9)+1)*9+j)==0 && Sivial.getvtotal().at((i%9+18)*9+j)==0 && Sivial.getvtotal().at((((i/9)/3)*3+(i%9)/3+27)*9+j)==0)
                while(Sivial.getvtotal().at((Sivial.getrowNo(i,j)))==0 && Sivial.getvtotal().at(Sivial.getcolNo(i,j))==0 && Sivial.getvtotal().at(Sivial.getcelNo(i,j))==0)
                //while(Sivial.getvtotal()[Sivial.getrowNo(i,j)]==0&&Sivial.getvtotal()[Sivial.getcolNo(i,j)]==0&&Sivial.getvtotal()[Sivial.getcolNo(i,j)]==0)
                {
                    //input 1
                    //vanscom[i]=1;
                    vanscom.at(i)=1;
                    //vanscom[Sivial.getrowNo(i,j)]=1;
                    vanscom.at(Sivial.getrowNo(i,j))=1;
                    //vanscom[Sivial.getcolNo(i,j)]=1;
                    vanscom.at(Sivial.getcolNo(i,j))=1;
                    //vanscom[Sivial.getcelNo(i,j)]=1;
                    vanscom.at(Sivial.getcelNo(i,j))=1;

                    cvanscom=vanscom;
                    /*
                    cvanscom[i]=vanscom[i];
                    cvanscom[Sivial.getrowNo(i,j)]=vanscom[Sivial.getrowNo(i,j)];
                    cvanscom[Sivial.getcolNo(i,j)]=vanscom[Sivial.getcolNo(i,j)];
                    cvanscom[Sivial.getcelNo(i,j)]=vanscom[Sivial.getcelNo(i,j)];
                    */

                    //become one of the possible ans
                    vansset.push_back(vanscom);

                    //change back to 0
                    //vanscom[i]=0;
                    vanscom.at(i)=0;
                    //vanscom[Sivial.getrowNo(i,j)]=1;
                    vanscom.at(Sivial.getrowNo(i,j))=0;
                    //vanscom[Sivial.getcolNo(i,j)]=1;
                    vanscom.at(Sivial.getcolNo(i,j))=0;
                    //vanscom[Sivial.getcelNo(i,j)]=1;
                    vanscom.at(Sivial.getcelNo(i,j))=0;
                    ++qans;//accumulate the nunber of possibly
                }
            }

            //input cutindex after all the possible ans is got
            vcutindex.push_back(qans);

            dis=i;//which square now
        }
    }
}

int Solver::discriminant()
{
    producer();
    if(qans==0)
    {
        return 2;//not valid possible answer for one of the square
    }
    else
    {
        return 1;//continue solving
    }
    //elementary();
}

void Solver::tuans()
{
    for(int d=0;d<81;++d)//finished solved,transform vcomset to array
    {
        for(int e=0;e<81;++e)//access only the square
        {
            //while (Sivial.getvcomset()[d][e]==1)
            while(Sivial.getvcomset().at(d).at(e)==1);
            {
                for(int f=1;f<10;++f)
                {
                    //while (Sivial.getvcomset()[d][((e/9)+8)*9-1+f == 1]
                    while(Sivial.getvcomset().at(d).at(((e/9)+8)*9-1+f)==1)
                    {
                        ans[e]=f;
                    }
                }
            }
        }
    }
}

void Solver::elementary()//add ans for those square which only have one possibly
{

    //noCol=0
    if(qans==1)//for those only have one possibly
    {
        addAns1(0);//add ans to vcomset in reader
        ++ndis;//accumulate how many square is filled
        Sivial.getvcomset().push_back(cvanscom);
    }
    else
    {
        if(count==0)
        {
            tuans();
        }
    }
    vansset.clear();//clear vansset
    vcutindex.clear();//clear vcutindex
    qans=0;//initialize qans
    count=0;//initialize count
}

void Solver::addAns1(int abc)//abc is the index in vansset, for square with 1 possibly ans, abc=0
{
    //Sivial.getvcomset().push_back(vansset[abc]
    Sivial.getvcomset().push_back(vansset.at(abc));//push the ans to the vcomset in reader
    for(int ac=0;ac<324;++ac)
    {
        //if(cvanscom[ac]==1)
        if(cvanscom.at(ac)==1)//only add the 1 in cvanscom, should only 4 quantity of 1
        {
            //Sivial.getvtotal()[ac]=cvanscom[ac];
            Sivial.getvtotal().at(ac)=cvanscom.at(ac);
        }
        //no need check whether the answer is valid or not as it must be correct
    }
}

void Solver::addAns2(std::vector <int> destination, std::vector <bool> ansAdded)//in tryrep,add two buffer, destination is the copy of vtotal in reader, ansAdded should be the copy of vansset
{
    Sivial.getvcomset().push_back(ansAdded);
    for(int ac=0;ac<324;++ac)
    {
       //while (ansAdded[ac]==1)
        while(ansAdded.at(ac)==1)
        {
           //destination[ac]=destination[ac]+ansAdded[ac];
           destination.at(ac)=destination.at(ac)+ansAdded.at(ac);//add/try the ans in ansAdded.at(ab)
        }
        //if(destination[ac]==2)
        if(destination.at(ac)==2)//check whether the ans is invalid or not
        {
           // while (ansAdded[ac]==1)
            while(ansAdded.at(ac)==1)
            {
                //destination[ac]=destination[ac]-ansAdded[ac];
                destination.at(ac)=destination.at(ac)-ansAdded.at(ac);//remove ans from destination
                Sivial.getvcomset().pop_back();//remove ansAdded from vcomset of reader
            }
                    /*
                    if(ind==index-1)
                    {
                        return 10;//10 means all possible ans is not valid, need to change the next possible of the ans before
                    }
                    */
        }
   }
}

int Solver::tryrep()
{
    producer();
    for(int i=0;i<324;++i)
    {
        //cvtotal[i]=Sivial.getvtotal()[i];
        cvtotal.push_back(Sivial.getvtotal().at(i));
    }
    int com=vcutindex.size();
    for(int ii=0;ii<com;++ii)//need how many for loops= how many square is empty=how many cutindex,ii is the loop index
    {
        int bufftotal;
        if(ii!=0)
        {
            for (int ite=0;ite<ii-1;++ite)
            {
                bufftotal+=vcutindex.at(ite);//bufftotal = summation of vcutindex before the loop in progress;
            }
        }
        else
        {
            bufftotal=0;
        }
        for (int jj=0;jj<bufftotal+vcutindex.at(ii);++jj)
        {
            std::vector <bool> vansbuff = vansset.at(jj);
            /*bool vansbuff[324] = {0};
            for (int kk=0;kk<324;++k)
            {
                vansbuff[kk] = (vansset.at(jj))[kk];
            }
            */
            addAns2(cvtotal,vansbuff);
            if(Sivial.getvcomset().size()==81)
            {
                tuans();
                return 1;
            }

        }
    }
    return 2;
}

int Solver::getans1()
{
    for(int i=0;i<81;++i)
    {
        return ans[i];
    }
}

void Solver::getans2()
{
    for(int i=0;i<81;++i)
    {
        std::cout<<ans[i]<<" ";
        while((i+1)%9==0)
        {
            std::cout<<std::endl;
        }
    }
}

void Solver::readerread()
{
    Sivial.readin();
}

void Solver::readertrans()
{
    Sivial.transform();
}
