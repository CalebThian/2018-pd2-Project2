#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QObject>
#include <QString>
#include <QTableWidget>
#include <QTableView>
#include <QToolButton>
#include <QPushButton>
#include "asolver.h"
#include "aissuer.h"
#include "areader.h"
#include "checker.h"
#include <cstdlib>
#include <cstdio>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    
public slots:
    void on_generate_clicked();
    void on_num1_clicked();
    void on_num2_clicked();
    void on_num3_clicked();
    void on_num4_clicked();
    void on_num5_clicked();
    void on_num6_clicked();
    void on_num7_clicked();
    void on_num8_clicked();
    void on_num9_clicked();

    void on_grid_0_clicked();
    void on_grid_1_clicked();
    void on_grid_2_clicked();
    void on_grid_3_clicked();
    void on_grid_4_clicked();
    void on_grid_5_clicked();
    void on_grid_6_clicked();
    void on_grid_7_clicked();
    void on_grid_8_clicked();
    void on_grid_9_clicked();

    void on_grid_10_clicked();
    void on_grid_11_clicked();
    void on_grid_12_clicked();
    void on_grid_13_clicked();
    void on_grid_14_clicked();
    void on_grid_15_clicked();
    void on_grid_16_clicked();
    void on_grid_17_clicked();
    void on_grid_18_clicked();
    void on_grid_19_clicked();

    void on_grid_20_clicked();
    void on_grid_21_clicked();
    void on_grid_22_clicked();
    void on_grid_23_clicked();
    void on_grid_24_clicked();
    void on_grid_25_clicked();
    void on_grid_26_clicked();
    void on_grid_27_clicked();
    void on_grid_28_clicked();
    void on_grid_29_clicked();

    void on_grid_30_clicked();
    void on_grid_31_clicked();
    void on_grid_32_clicked();
    void on_grid_33_clicked();
    void on_grid_34_clicked();
    void on_grid_35_clicked();
    void on_grid_36_clicked();
    void on_grid_37_clicked();
    void on_grid_38_clicked();
    void on_grid_39_clicked();

    void on_grid_40_clicked();
    void on_grid_41_clicked();
    void on_grid_42_clicked();
    void on_grid_43_clicked();
    void on_grid_44_clicked();
    void on_grid_45_clicked();
    void on_grid_46_clicked();
    void on_grid_47_clicked();
    void on_grid_48_clicked();
    void on_grid_49_clicked();

    void on_grid_50_clicked();
    void on_grid_51_clicked();
    void on_grid_52_clicked();
    void on_grid_53_clicked();
    void on_grid_54_clicked();
    void on_grid_55_clicked();
    void on_grid_56_clicked();
    void on_grid_57_clicked();
    void on_grid_58_clicked();
    void on_grid_59_clicked();

    void on_grid_60_clicked();
    void on_grid_61_clicked();
    void on_grid_62_clicked();
    void on_grid_63_clicked();
    void on_grid_64_clicked();
    void on_grid_65_clicked();
    void on_grid_66_clicked();
    void on_grid_67_clicked();
    void on_grid_68_clicked();
    void on_grid_69_clicked();

    void on_grid_70_clicked();
    void on_grid_71_clicked();
    void on_grid_72_clicked();
    void on_grid_73_clicked();
    void on_grid_74_clicked();
    void on_grid_75_clicked();
    void on_grid_76_clicked();
    void on_grid_77_clicked();
    void on_grid_78_clicked();
    void on_grid_79_clicked();

    void on_grid_80_clicked();


private slots:


    void on_check_clicked();

    void on_solve_clicked();

    void on_custom_clicked();

private:
    Ui::MainWindow *ui;
    bool modCustom;
    QString num;
    QString sudoku[81];
    int Ans[81];
    Aissuer Paul;
    Checker Nathan;
    Areader Kenny;
    Asolver Mae;
};

#endif // MAINWINDOW_H
