#ifndef ASOLVER_H
#define ASOLVER_H
#include "areader.h"
#include "checker.h"
#include <vector>
#include <cstdlib>
#include <ctime>
#include <algorithm>

class Asolver
{
public:
    Asolver();
    void getQues();
    void getPossible();
    void readerRead();
    bool posRow(int p,int q);
    bool posCol(int p,int q);
    bool posCel(int p,int q);
    bool posNum(int p,int q);
    bool check(int arr2[]);
    bool checkAbs(int arr2[]);
    int ques[Areader::Su_size];
private:

    //std::vector< std::vector <int> > Ans;
    Areader Sivial;
    Checker Judge;
    bool correctAns;
};

#endif // ASOLVER_H
