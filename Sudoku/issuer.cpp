#include "issuer.h"

Issuer::Issuer()
{
    for(int i=0;i<81;++i)
    {
        squaIndex.push_back(i);
    }
    for(int i=0;i<1000;++i)//Shuffling algorithm
    {
        int m = rand()%81;
        int n = rand()%81;
        int tempo=squaIndex.at(m);
        squaIndex.at(m) = squaIndex.at(n);
        squaIndex.at(n) = tempo;
    }
    for(int i=0;i<81;++i)
    {
        ques[i]=0;
    }
    for(int i=0;i<324;++i)
    {
        this->element.push_back(0);
        this->questotal.push_back(0);
    }
}

void Issuer::genElement()
{
    int fill = rand()%40+25;
    for(int i=0;i<fill+1;++i)
    {
        int a = squaIndex.at(i);
        int b = rand()%9;
        int rowNo=((a/9)+9)*9+b;
        int colNo=((a%9)+18)*9+b;
        int celNo=(((a/9)/3)*3+(a%9)/3+27)*9+b;
        if(questotal.at(a)==1||questotal.at(rowNo)==1||questotal.at(colNo)==1||questotal.at(celNo)==1)
        {
            ++fill;
        }
        else
        {
            element.at(i)=1;
            element.at(rowNo)=1;
            element.at(colNo)=1;
            element.at(celNo)=1;
            quesset.push_back(element);
            questotal.at(i)=1;
            questotal.at(rowNo)=1;
            questotal.at(colNo)=1;
            questotal.at(celNo)=1;
            element.at(i)=0;
            element.at(rowNo)=0;
            element.at(colNo)=0;
            element.at(celNo)=0;
        }
    }
    int size=quesset.size();
    for(int d=0;d<size;++d)//finished generate,transform quesset to array
    {
        for(int e=0;e<81;++e)//access only the square
        {
            while(quesset.at(d).at(e)==1);//while the first one of this element
            {
                for(int f=0;f<9;++f)//get the number filled at it
                {
                    while(quesset.at(d).at(((e/9)+1)*9+f)==1)//by row to get the number filled
                    {
                           ques[e]=f;
                    }
                }
            }
        }
    }
}

void Issuer::sudokuGen()
{
    for(int i=0;i<81;++i)
    {
        if(i%9!=0)
            std::cout<<" ";
        if(i%9==8)
            std::cout<<std::endl;
        if(ques[i]==0)
        {
            std::cout<<"   ";
        }
        else
        {
            std::cout<<ques[i];
        }
    }
}
