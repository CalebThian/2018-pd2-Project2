#include "aissuer.h"

Aissuer::Aissuer()
{
   int copy[3][81]={{4,3,5,2,6,9,7,8,1,
                   6,8,2,5,7,1,4,9,3,
                   1,9,7,8,3,4,5,6,2,
                   8,2,6,1,9,5,3,4,7,
                   3,7,4,6,8,2,9,1,5,
                   9,5,1,7,4,3,6,2,8,
                   5,1,9,3,2,6,8,7,4,
                   2,4,8,9,5,7,1,3,6,
                   7,6,3,4,1,8,2,5,9},
                   {1,5,2,4,8,9,3,7,6,
                   7,3,9,2,5,6,8,4,1,
                   4,6,8,3,7,1,2,9,5,
                   3,8,7,1,2,4,6,5,9,
                   5,9,1,7,6,3,4,2,8,
                   2,4,6,8,9,5,7,1,3,
                   9,1,4,6,3,7,5,8,2,
                   6,2,5,9,4,8,1,3,7,
                   8,7,3,5,1,2,9,6,4},
                   {8,1,2,7,5,3,6,4,9,
                   9,4,3,6,8,2,1,7,5,
                   6,7,5,4,9,1,2,8,3,
                   1,5,4,2,3,7,8,9,6,
                   3,6,9,8,4,5,7,2,1,
                   2,8,7,1,6,9,5,3,4,
                   5,2,1,9,7,4,3,6,8,
                   4,3,8,5,2,6,9,1,7,
                   7,9,6,3,1,8,4,5,2}};
   for(int i=0;i<3;++i)
   {
       for(int j=0;j<81;++j)
       {
           ques1[i][j]=copy[i][j];
       }
   }
     srand(time(NULL));
}


void Aissuer::sudokuGen()
{
    for(int i=0;i<81;++i)
        ques[i]=0;
    int fill = rand()%45+25;
    int index[81];
    int rand4=rand()%3;
    for(int a=0;a<81;++a)
        ques[a]=ques1[rand4][a];
    for(int i=0;i<81;++i)
        index[i]=i;
    for(int i=0;i<1000;++i)
     {
        int rand1 = rand()%81;
        int rand2 = rand()%81;
        int temp = index[rand1];
        index[rand1] = index[rand2];
        index[rand2] =temp;
    }
    for(int i=0;i<81-fill;++i)
    {
      int rand3=index[i];
      ques[rand3]=0;
    }

    //here is print out


   /*
   for(int i=0;i<81;++i)
    {
        if(i%9!=0)
            std::cout<<" ";
        if(ques[i]!=0)
            std::cout<<ques[i];
        else
            std::cout<<"0";
        if(i%9==8)
            std::cout<<std::endl;
    }
    */
}

bool Aissuer::checkUnity(int arr[])
{
    int arr_unity[9];

    for(int ci=0;ci<9;++ci)
    {
        arr_unity[ci]=0;//initialize arr_unity to {0}
    }
    for(int ci=0;ci<9;++ci)
    {
        if(arr[ci]>0)
            ++arr_unity[arr[ci]-1];//if arr[i] have number 1~9, such as 8, then arr_unity[7] will become 1 from 0
    }
    for(int ci=0;ci<9;++ci)
    {
        if(arr_unity[ci]>1)//not 1~9 is inside arr[]
        {
            return false;
        }
    }
     return true;
}

bool Aissuer::isCorrect(int arr2[81])
{
    bool check_result;
    int check_arr[9];
    int location;

    //check row
    for(int ci=0;ci<81;ci+=9)
    {
        for(int cj=0;cj<9;++cj)
        {
            check_arr[cj] = arr2[ci+cj];
        }
        check_result = checkUnity(check_arr);
        if(check_result==false)
        {
            return false;
        }
    }

    //check columns
    for(int ci=0;ci<9;++ci)
    {
        for(int cj=0;cj<9;++cj)
        {
            check_arr[cj]=arr2[ci+cj*9];//such as 0,9,18,27......
        }
        check_result = checkUnity(check_arr);
        if(check_result==false)
        {
            return false;
        }
    }

    //check cells
    for(int ci=0;ci<9;++ci)
    {
        for(int cj=0;cj<9;++cj)
        {
            location = 27*(ci/3)+3*(ci%3)+9*(cj/3)+(cj%3);
            check_arr[cj]=arr2[location];
        }
        check_result=checkUnity(check_arr);
        if(check_result==false)
        {
            return false;
        }
    }
    return true;
}
