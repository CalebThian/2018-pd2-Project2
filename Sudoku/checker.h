#ifndef CHECKER_H
#define CHECKER_H


class Checker
{
public:
    Checker();
    bool isCorrect(int arr2[81]);
    bool isCorrectAbs(int arr2[81]);


private:
    bool checkUnity(int arr[]);
    bool checkUnityAbs(int arr[]);
};

#endif // CHECKER_H
