#include "reader.h"


Reader::Reader()
{
    //bool vcomponent[324]={0};
    std::vector <bool> vcomponent(324,0);
    //bool vector[324]={0};
    std::vector <bool> vtotal(324,0);
}

int Reader::getrowNo(int a, int b)
{
    int ans=((a/9)+9)*9-1+b;
    return ans;
}

int Reader::getcolNo(int c, int d)
{
    int ans=((c%9)+18)*9-1+d;
    return ans;
}

int Reader::getcelNo(int e, int f)
{
    int ans=(((e/9)/3)*3+(e%9)/3+27)*9-1+f;
    return ans;
}

void Reader::readin()//read a Sudoku, without define sudoku form(like vector or array)
{
    for (int i=0; i<Su_size; ++i)
    {
        std::cin>>Su[i];
    }
}


void Reader::transform()//Get vcomset
{
    for(int i=0; i<81; ++i)
    {

        // transform every non zero number in the Sudoku into vcomponent with four 1

        //vcomponent[i]=1;
        vcomponent.at(i)=1;//0~80, store which square of the number is
        //vcomponent[getrowNo(i,Su[i])]=1;
        vcomponent.at(getrowNo(i,Su[i]))=1;//81~161, store which row is the number is and what is it number
        //vcomponent[getcolNo(i,Su[i])]=1;
        vcomponent.at(getcolNo(i,Su[i]))=1;//162~242, store which column is the number is and what is it number
        //vcomponent[getcelNo(i,Su[i])]=1;
        vcomponent.at(getcelNo(i,Su[i]))=1;//283~323, store which cell is the number is and what is it number

         //save vcomponent to vtotal, vtotal should have (quantity of number given)*4 of 1

        /*
         for(int k=0;k<324;++k)
        {
            vtotal[i]=vcomponent[i];//vtotal.at(i)=vcomponent.at(i);
        }
        */


         //save all the vcomponent to vcomset
        vcomset.push_back(vcomponent);
        /*
         let vcomponent become vector filled with 0
         */
        //vcomponent[i]=0;
        vcomponent.at(i)=0;
        //vcomponent[getrowNo(i,Su[i])]=0;
        vcomponent.at(getrowNo(i,Su[i]))=0;
        //vcomponent[getcolNo(i,Su[i])]=1;
        vcomponent.at(getcolNo(i,Su[i]))=0;
        //vcomponent[getcelNo(i,Su[i])]=1;
        vcomponent.at(getcelNo(i,Su[i]))=0;
    };
}

//bool Reader::getvtotal()
std::vector <bool> Reader::getvtotal()//get vtotal
{
    return vtotal;
}

//std::vector <bool> Reader::getvcomset()
std::vector <std::vector <bool> > Reader::getvcomset()
{
    return vcomset;
}
