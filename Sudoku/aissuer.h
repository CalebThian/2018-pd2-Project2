#ifndef AISSUER_H
#define AISSUER_H
#include <iostream>
#include "checker.h"
#include <cstdlib>
#include <ctime>
#include "asolver.h"

class Aissuer
{
public:
    Aissuer();
    friend class MainWindow;
    void sudokuGen();
    bool isCorrect(int arr2[81]);
    int ques[81];
private:
    bool checkUnity(int arr[]);
    int ques1[3][81];

};

#endif // AISSUER_H
