#ifndef READER_H
#define READER_H
#include <iostream>
#include <vector>
#include <fstream>

class Reader
{
public:
    Reader();
    int getrowNo(int a, int b);
    int getcolNo(int c, int d);
    int getcelNo(int e, int f);
    void readin();
    //void readin2();
    void transform();
    /*bool getvtotal();*/std::vector <bool> getvtotal();
    std::vector </*bool*/ std::vector <bool> > getvcomset();
    const static int Su_size=81;
private:
    int Su[Su_size];
    /*bool vcomponent;*/std::vector<bool> vcomponent;
    std::vector< /*bool*/ std::vector <bool> > vcomset ;//vcomset is the set of sudoku component
    /*bool vtotal;*/std::vector<bool> vtotal;

};

#endif // READER_H
