#ifndef AREADER_H
#define AREADER_H
#include <iostream>

class Areader
{
public:
    Areader();
    friend class Asolver;
    void read();
    static const int Su_size=81;
private:
    int Sudoku[Su_size];

};

#endif // AREADER_H
