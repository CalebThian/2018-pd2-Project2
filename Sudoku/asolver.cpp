﻿#include "asolver.h"

Asolver::Asolver()
{
    correctAns = false;
}

void Asolver::readerRead()
{
    Sivial.read();
}

void Asolver::getQues()
{
    for (int i=0; i<81;++i)
    {
        this->ques[i]=Sivial.Sudoku[i];
    }

}

void Asolver::getPossible()
{
    correctAns=false;
    std::vector<std::vector<int> > Ans(81);
   int checkEmpty=0;
   if(check(ques)==false)
   {
        std::cout<<"Unsolvable"<<std::endl;
        return;
   }

   while(1)
   {
       int count=0;
       checkEmpty=0;
       for(int clc=0;clc<81;++clc)
           Ans.at(clc).clear();
        for(int i=0;i<81;++i)//i is index
        {
           if(this->ques[i]==0)//while square i is empty, process this square
           {
                ++checkEmpty;//empty square quantity ++
                for(int poss=1;poss<10;++poss)
                {
                    if(posNum(i,poss)==true)
                    {
                        //std::cout<<"This is poss: "<<poss<<std::endl;
                        Ans.at(i).push_back(poss);
                        //std::cout<<"This is poss: "<<poss<<std::endl;
                    }
                }
                if(Ans.at(i).size()==1)//only a possible ans
                {
                    ++count;
                    this->ques[i]=Ans.at(i).at(0);
                }
                if(Ans.at(i).size()==0)//if some empty square with no possible ans
                {
                    std::cout<<"No SOlution"<<std::endl;
                    std::cout<<"i is : " <<i<<std::endl;
                    for(int a=0;a<Areader::Su_size;++a)
                    {
                        std::cout<<this->ques[a]<<" ";
                        if(a%9==8)
                        {
                            std::cout<<std::endl;
                        }
                    }
                    return;
                }

           }
        }
        if(count==0)
            break;
   }




        if(checkEmpty==0)
            {
                for(int jj=0;jj<81;++jj)
                {
                    if(ques[jj]==0)
                    {
                         for(int poss=1;poss<10;++poss)
                         {
                             if(posNum(jj,poss)==true)
                             {
                              Ans.at(jj).push_back(poss);
                             }
                         }
                    }
                    if(Ans.at(jj).size()==1)
                    {
                        this->ques[jj]=Ans.at(jj).at(0);
                    }
                }
                correctAns=true;
                //cout ans

                std::cout<<"Solvable"<<std::endl;
                for(int a=0;a<Areader::Su_size;++a)
                {
                    std::cout<<this->ques[a]<<" ";
                    if(a%9==8)
                    {
                        std::cout<<std::endl;
                    }
                }

                return;
            }
            else
            {
               for(int ii=0;ii<Areader::Su_size;++ii)
               {
                   if(ques[ii]==0)
                   {
                       for(int clc=0;clc<81;++clc)
                           Ans.at(clc).clear();
                       for(int poss=1;poss<10;++poss)
                       {
                         if(posNum(ii,poss)==true)
                             Ans.at(ii).push_back(poss);
                       }
                       int tempo[81];
                       for(int kk=0;kk<Areader::Su_size;++kk)
                           tempo[kk]=ques[kk];
                       for (int kk=0;kk<Ans.at(ii).size();++kk)
                       {
                           for(int k=0;k<Areader::Su_size;++k)
                                ques[k]=tempo[k];

                           ques[ii]=Ans.at(ii).at(kk);

                           std::cout<< Ans.at(ii).size()<<" "<<ques[ii]<<" and"<<ii<<" "<<correctAns<<std::endl;
                           getPossible();

                           std::cout<< Ans.at(ii).size()<<" "<<ques[ii]<<" and"<<ii<<" "<<correctAns<<std::endl;
                           if(correctAns==true)
                              return;
                       }
                  }
              }
            }

   }


bool Asolver::posRow(int p, int q)
{
    for (int r=0;r<9;++r)
    {
        if(ques[(p/9)*9+r]==q)
        {
            return false;
        }
    }
    return true;
}

bool Asolver::posCol(int p, int q)
{
    for(int r=0;r<9;++r)
    {
        if(ques[(p%9)+9*r]==q)
        {
            return false;
        }
    }
    return true;
}

bool Asolver::posCel(int p, int q)
{
    int location = ((p/9)/3)*3+(p%9)/3;
    for(int r=0;r<9;++r)
    {
        if(ques[27*(location/3)+3*(location%3)+9*(r/3)+(r%3)]==q)
        {
            return false;
        }
    }
    return true;
}

bool Asolver::posNum(int p, int q)
{
    return posRow(p,q) && posCol(p,q) && posCel(p,q);
}

bool Asolver::check(int arr[])
{
    if(Judge.isCorrect(arr)==false)
    {
        return false;
    }
    else
    {
        return true;
    }
}
