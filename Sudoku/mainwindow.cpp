#include "mainwindow.h"
#include "ui_mainwindow.h"

#define A(num) connect(ui->grid_##num,SIGNAL(clicked(bool)),this,SLOT(on_grid_##num##_clicked()))
#define inNum(n)  connect(ui->num##n,SIGNAL(clicked(bool)),this,SLOT(on_num##n##_clicked()))
#define cliNum(n) on_num##n##_clicked(){     \
    num=QString (#n);                        \
    ui->label->setText(num);   }
#define priNum(n) on_grid_##n##_clicked()   \
{                                           \
    if(modCustom==0)                        \
        ui->grid_##n->setText(num);         \
    else if(modCustom==1){                  \
        if(ui->grid_##n->text()==" ")       \
            ui->grid_##n->setText(num);}}
#define getSu(n) sudoku[n]=ui->grid_##n->text()\


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    modCustom=0;
    num=QString("0");
    connect(ui->generate,SIGNAL(clicked(bool)),this,SLOT(on_generate_clicked()));
    connect(ui->check,SIGNAL(clicked(bool)),this,SLOT(on_check_clicked()));
    A(0);
    A(1);
    A(2);
    A(3);
    A(4);
    A(5);
    A(6);
    A(7);
    A(8);
    A(9);

    A(10);
    A(11);
    A(12);
    A(13);
    A(14);
    A(15);
    A(16);
    A(17);
    A(18);
    A(19);

    A(20);
    A(21);
    A(22);
    A(23);
    A(24);
    A(25);
    A(26);
    A(27);
    A(28);
    A(29);

    A(30);
    A(31);
    A(32);
    A(33);
    A(34);
    A(35);
    A(36);
    A(37);
    A(38);
    A(39);

    A(40);
    A(41);
    A(42);
    A(43);
    A(44);
    A(45);
    A(46);
    A(47);
    A(48);
    A(49);

    A(50);
    A(51);
    A(52);
    A(53);
    A(54);
    A(55);
    A(56);
    A(57);
    A(58);
    A(59);

    A(60);
    A(61);
    A(62);
    A(63);
    A(64);
    A(65);
    A(66);
    A(67);
    A(68);
    A(69);

    A(70);
    A(71);
    A(72);
    A(73);
    A(74);
    A(75);
    A(76);
    A(77);
    A(78);
    A(79);

    A(80);

//    connect(ui->grid_0,SIGNAL(clicked(bool)),this,SLOT(on_grid_0_clicked()));

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::cliNum(1)
void MainWindow::cliNum(2)
void MainWindow::cliNum(3)
void MainWindow::cliNum(4)
void MainWindow::cliNum(5)
void MainWindow::cliNum(6)
void MainWindow::cliNum(7)
void MainWindow::cliNum(8)
void MainWindow::cliNum(9)

void MainWindow::priNum(0)
void MainWindow::priNum(1)
void MainWindow::priNum(2)
void MainWindow::priNum(3)
void MainWindow::priNum(4)
void MainWindow::priNum(5)
void MainWindow::priNum(6)
void MainWindow::priNum(7)
void MainWindow::priNum(8)
void MainWindow::priNum(9)

void MainWindow::priNum(10)
void MainWindow::priNum(11)
void MainWindow::priNum(12)
void MainWindow::priNum(13)
void MainWindow::priNum(14)
void MainWindow::priNum(15)
void MainWindow::priNum(16)
void MainWindow::priNum(17)
void MainWindow::priNum(18)
void MainWindow::priNum(19)

void MainWindow::priNum(20)
void MainWindow::priNum(21)
void MainWindow::priNum(22)
void MainWindow::priNum(23)
void MainWindow::priNum(24)
void MainWindow::priNum(25)
void MainWindow::priNum(26)
void MainWindow::priNum(27)
void MainWindow::priNum(28)
void MainWindow::priNum(29)

void MainWindow::priNum(30)
void MainWindow::priNum(31)
void MainWindow::priNum(32)
void MainWindow::priNum(33)
void MainWindow::priNum(34)
void MainWindow::priNum(35)
void MainWindow::priNum(36)
void MainWindow::priNum(37)
void MainWindow::priNum(38)
void MainWindow::priNum(39)

void MainWindow::priNum(40)
void MainWindow::priNum(41)
void MainWindow::priNum(42)
void MainWindow::priNum(43)
void MainWindow::priNum(44)
void MainWindow::priNum(45)
void MainWindow::priNum(46)
void MainWindow::priNum(47)
void MainWindow::priNum(48)
void MainWindow::priNum(49)

void MainWindow::priNum(50)
void MainWindow::priNum(51)
void MainWindow::priNum(52)
void MainWindow::priNum(53)
void MainWindow::priNum(54)
void MainWindow::priNum(55)
void MainWindow::priNum(56)
void MainWindow::priNum(57)
void MainWindow::priNum(58)
void MainWindow::priNum(59)

void MainWindow::priNum(60)
void MainWindow::priNum(61)
void MainWindow::priNum(62)
void MainWindow::priNum(63)
void MainWindow::priNum(64)
void MainWindow::priNum(65)
void MainWindow::priNum(66)
void MainWindow::priNum(67)
void MainWindow::priNum(68)
void MainWindow::priNum(69)

void MainWindow::priNum(70)
void MainWindow::priNum(71)
void MainWindow::priNum(72)
void MainWindow::priNum(73)
void MainWindow::priNum(74)
void MainWindow::priNum(75)
void MainWindow::priNum(76)
void MainWindow::priNum(77)
void MainWindow::priNum(78)
void MainWindow::priNum(79)

void MainWindow::priNum(80)

void MainWindow::on_generate_clicked()
{
    ui->label_5->setText("");
    ui->label_2->setText("");
    modCustom=1;
    ui->label_5->setText("");
    Paul.sudokuGen();
    for(int i=0;i<81;++i)
        std::cout<<Paul.ques[i]<<" ";
    std::cout<<std::endl;
    for(int i=0;i<81;++i)
    {
        if(Paul.ques[i]!=0)
        {
            char NO[2];
            sprintf(NO,"%d",Paul.ques[i]);
            sudoku[i]=QString (NO);
        }
        else if(Paul.ques[i]==0)
            sudoku[i]=QString (" ");
    }
    ui->grid_0->setText(sudoku[0]);
    ui->grid_1->setText(sudoku[1]);
    ui->grid_2->setText(sudoku[2]);
    ui->grid_3->setText(sudoku[3]);
    ui->grid_4->setText(sudoku[4]);
    ui->grid_5->setText(sudoku[5]);
    ui->grid_6->setText(sudoku[6]);
    ui->grid_7->setText(sudoku[7]);
    ui->grid_8->setText(sudoku[8]);
    ui->grid_9->setText(sudoku[9]);

    ui->grid_10->setText(sudoku[10]);
    ui->grid_11->setText(sudoku[11]);
    ui->grid_12->setText(sudoku[12]);
    ui->grid_13->setText(sudoku[13]);
    ui->grid_14->setText(sudoku[14]);
    ui->grid_15->setText(sudoku[15]);
    ui->grid_16->setText(sudoku[16]);
    ui->grid_17->setText(sudoku[17]);
    ui->grid_18->setText(sudoku[18]);
    ui->grid_19->setText(sudoku[19]);

    ui->grid_20->setText(sudoku[20]);
    ui->grid_21->setText(sudoku[21]);
    ui->grid_22->setText(sudoku[22]);
    ui->grid_23->setText(sudoku[23]);
    ui->grid_24->setText(sudoku[24]);
    ui->grid_25->setText(sudoku[25]);
    ui->grid_26->setText(sudoku[26]);
    ui->grid_27->setText(sudoku[27]);
    ui->grid_28->setText(sudoku[28]);
    ui->grid_29->setText(sudoku[29]);

    ui->grid_30->setText(sudoku[30]);
    ui->grid_31->setText(sudoku[31]);
    ui->grid_32->setText(sudoku[32]);
    ui->grid_33->setText(sudoku[33]);
    ui->grid_34->setText(sudoku[34]);
    ui->grid_35->setText(sudoku[35]);
    ui->grid_36->setText(sudoku[36]);
    ui->grid_37->setText(sudoku[37]);
    ui->grid_38->setText(sudoku[38]);
    ui->grid_39->setText(sudoku[39]);

    ui->grid_40->setText(sudoku[40]);
    ui->grid_41->setText(sudoku[41]);
    ui->grid_42->setText(sudoku[42]);
    ui->grid_43->setText(sudoku[43]);
    ui->grid_44->setText(sudoku[44]);
    ui->grid_45->setText(sudoku[45]);
    ui->grid_46->setText(sudoku[46]);
    ui->grid_47->setText(sudoku[47]);
    ui->grid_48->setText(sudoku[48]);
    ui->grid_49->setText(sudoku[49]);

    ui->grid_50->setText(sudoku[50]);
    ui->grid_51->setText(sudoku[51]);
    ui->grid_52->setText(sudoku[52]);
    ui->grid_53->setText(sudoku[53]);
    ui->grid_54->setText(sudoku[54]);
    ui->grid_55->setText(sudoku[55]);
    ui->grid_56->setText(sudoku[56]);
    ui->grid_57->setText(sudoku[57]);
    ui->grid_58->setText(sudoku[58]);
    ui->grid_59->setText(sudoku[59]);

    ui->grid_60->setText(sudoku[60]);
    ui->grid_61->setText(sudoku[61]);
    ui->grid_62->setText(sudoku[62]);
    ui->grid_63->setText(sudoku[63]);
    ui->grid_64->setText(sudoku[64]);
    ui->grid_65->setText(sudoku[65]);
    ui->grid_66->setText(sudoku[66]);
    ui->grid_67->setText(sudoku[67]);
    ui->grid_68->setText(sudoku[68]);
    ui->grid_69->setText(sudoku[69]);

    ui->grid_70->setText(sudoku[70]);
    ui->grid_71->setText(sudoku[71]);
    ui->grid_72->setText(sudoku[72]);
    ui->grid_73->setText(sudoku[73]);
    ui->grid_74->setText(sudoku[74]);
    ui->grid_75->setText(sudoku[75]);
    ui->grid_76->setText(sudoku[76]);
    ui->grid_77->setText(sudoku[77]);
    ui->grid_78->setText(sudoku[78]);
    ui->grid_79->setText(sudoku[79]);

    ui->grid_80->setText(sudoku[80]);


}

void MainWindow::on_check_clicked()
{
    ui->label_5->setText("");
    ui->label_2->setText("");
    getSu(0);
    getSu(1);
    getSu(2);
    getSu(3);
    getSu(4);
    getSu(5);
    getSu(6);
    getSu(7);
    getSu(8);
    getSu(9);

    getSu(10);
    getSu(11);
    getSu(12);
    getSu(13);
    getSu(14);
    getSu(15);
    getSu(16);
    getSu(17);
    getSu(18);
    getSu(19);

    getSu(20);
    getSu(21);
    getSu(22);
    getSu(23);
    getSu(24);
    getSu(25);
    getSu(26);
    getSu(27);
    getSu(28);
    getSu(29);

    getSu(30);
    getSu(31);
    getSu(32);
    getSu(33);
    getSu(34);
    getSu(35);
    getSu(36);
    getSu(37);
    getSu(38);
    getSu(39);

    getSu(40);
    getSu(41);
    getSu(42);
    getSu(43);
    getSu(44);
    getSu(45);
    getSu(46);
    getSu(47);
    getSu(48);
    getSu(49);

    getSu(50);
    getSu(51);
    getSu(52);
    getSu(53);
    getSu(54);
    getSu(55);
    getSu(56);
    getSu(57);
    getSu(58);
    getSu(59);

    getSu(60);
    getSu(61);
    getSu(62);
    getSu(63);
    getSu(64);
    getSu(65);
    getSu(66);
    getSu(67);
    getSu(68);
    getSu(69);

    getSu(70);
    getSu(71);
    getSu(72);
    getSu(73);
    getSu(74);
    getSu(75);
    getSu(76);
    getSu(77);
    getSu(78);
    getSu(79);
    getSu(80);

    for(int i=0;i<81;++i)
        Ans[i]=sudoku[i].toInt();
    if(Nathan.isCorrectAbs(Ans)==true)
           ui->label_2->setText("Correct");
    else
           ui->label_2->setText("Not correct");
}

void MainWindow::on_solve_clicked()
{
   ui->label_5->setText("");
   ui->label_2->setText("");
    getSu(0);
    getSu(1);
    getSu(2);
    getSu(3);
    getSu(4);
    getSu(5);
    getSu(6);
    getSu(7);
    getSu(8);
    getSu(9);

    getSu(10);
    getSu(11);
    getSu(12);
    getSu(13);
    getSu(14);
    getSu(15);
    getSu(16);
    getSu(17);
    getSu(18);
    getSu(19);

    getSu(20);
    getSu(21);
    getSu(22);
    getSu(23);
    getSu(24);
    getSu(25);
    getSu(26);
    getSu(27);
    getSu(28);
    getSu(29);

    getSu(30);
    getSu(31);
    getSu(32);
    getSu(33);
    getSu(34);
    getSu(35);
    getSu(36);
    getSu(37);
    getSu(38);
    getSu(39);

    getSu(40);
    getSu(41);
    getSu(42);
    getSu(43);
    getSu(44);
    getSu(45);
    getSu(46);
    getSu(47);
    getSu(48);
    getSu(49);

    getSu(50);
    getSu(51);
    getSu(52);
    getSu(53);
    getSu(54);
    getSu(55);
    getSu(56);
    getSu(57);
    getSu(58);
    getSu(59);

    getSu(60);
    getSu(61);
    getSu(62);
    getSu(63);
    getSu(64);
    getSu(65);
    getSu(66);
    getSu(67);
    getSu(68);
    getSu(69);

    getSu(70);
    getSu(71);
    getSu(72);
    getSu(73);
    getSu(74);
    getSu(75);
    getSu(76);
    getSu(77);
    getSu(78);
    getSu(79);
    getSu(80);

    for(int i=0;i<81;++i)
        Ans[i]=sudoku[i].toInt();
    for(int i=0;i<81;++i)
        Mae.ques[i]=Ans[i];
    Mae.getPossible();
    for(int i=0;i<81;++i)
    {
        if(Mae.ques[i]==0&&modCustom==0)
         {
            ui->label_5->setText("Does not have solution");
            return;
        }
    }
    if(modCustom==0)
        ui->label_5->setText("With Solution");
    for(int i=0;i<81;++i)
    {
        char NO[2];
        sprintf(NO,"%d",Mae.ques[i]);
        sudoku[i]=QString (NO);
    }
    ui->grid_0->setText(sudoku[0]);
    ui->grid_1->setText(sudoku[1]);
    ui->grid_2->setText(sudoku[2]);
    ui->grid_3->setText(sudoku[3]);
    ui->grid_4->setText(sudoku[4]);
    ui->grid_5->setText(sudoku[5]);
    ui->grid_6->setText(sudoku[6]);
    ui->grid_7->setText(sudoku[7]);
    ui->grid_8->setText(sudoku[8]);
    ui->grid_9->setText(sudoku[9]);

    ui->grid_10->setText(sudoku[10]);
    ui->grid_11->setText(sudoku[11]);
    ui->grid_12->setText(sudoku[12]);
    ui->grid_13->setText(sudoku[13]);
    ui->grid_14->setText(sudoku[14]);
    ui->grid_15->setText(sudoku[15]);
    ui->grid_16->setText(sudoku[16]);
    ui->grid_17->setText(sudoku[17]);
    ui->grid_18->setText(sudoku[18]);
    ui->grid_19->setText(sudoku[19]);

    ui->grid_20->setText(sudoku[20]);
    ui->grid_21->setText(sudoku[21]);
    ui->grid_22->setText(sudoku[22]);
    ui->grid_23->setText(sudoku[23]);
    ui->grid_24->setText(sudoku[24]);
    ui->grid_25->setText(sudoku[25]);
    ui->grid_26->setText(sudoku[26]);
    ui->grid_27->setText(sudoku[27]);
    ui->grid_28->setText(sudoku[28]);
    ui->grid_29->setText(sudoku[29]);

    ui->grid_30->setText(sudoku[30]);
    ui->grid_31->setText(sudoku[31]);
    ui->grid_32->setText(sudoku[32]);
    ui->grid_33->setText(sudoku[33]);
    ui->grid_34->setText(sudoku[34]);
    ui->grid_35->setText(sudoku[35]);
    ui->grid_36->setText(sudoku[36]);
    ui->grid_37->setText(sudoku[37]);
    ui->grid_38->setText(sudoku[38]);
    ui->grid_39->setText(sudoku[39]);

    ui->grid_40->setText(sudoku[40]);
    ui->grid_41->setText(sudoku[41]);
    ui->grid_42->setText(sudoku[42]);
    ui->grid_43->setText(sudoku[43]);
    ui->grid_44->setText(sudoku[44]);
    ui->grid_45->setText(sudoku[45]);
    ui->grid_46->setText(sudoku[46]);
    ui->grid_47->setText(sudoku[47]);
    ui->grid_48->setText(sudoku[48]);
    ui->grid_49->setText(sudoku[49]);

    ui->grid_50->setText(sudoku[50]);
    ui->grid_51->setText(sudoku[51]);
    ui->grid_52->setText(sudoku[52]);
    ui->grid_53->setText(sudoku[53]);
    ui->grid_54->setText(sudoku[54]);
    ui->grid_55->setText(sudoku[55]);
    ui->grid_56->setText(sudoku[56]);
    ui->grid_57->setText(sudoku[57]);
    ui->grid_58->setText(sudoku[58]);
    ui->grid_59->setText(sudoku[59]);

    ui->grid_60->setText(sudoku[60]);
    ui->grid_61->setText(sudoku[61]);
    ui->grid_62->setText(sudoku[62]);
    ui->grid_63->setText(sudoku[63]);
    ui->grid_64->setText(sudoku[64]);
    ui->grid_65->setText(sudoku[65]);
    ui->grid_66->setText(sudoku[66]);
    ui->grid_67->setText(sudoku[67]);
    ui->grid_68->setText(sudoku[68]);
    ui->grid_69->setText(sudoku[69]);

    ui->grid_70->setText(sudoku[70]);
    ui->grid_71->setText(sudoku[71]);
    ui->grid_72->setText(sudoku[72]);
    ui->grid_73->setText(sudoku[73]);
    ui->grid_74->setText(sudoku[74]);
    ui->grid_75->setText(sudoku[75]);
    ui->grid_76->setText(sudoku[76]);
    ui->grid_77->setText(sudoku[77]);
    ui->grid_78->setText(sudoku[78]);
    ui->grid_79->setText(sudoku[79]);

    ui->grid_80->setText(sudoku[80]);

}



void MainWindow::on_custom_clicked()
{
    ui->label_5->setText("");
    ui->label_2->setText("");
    modCustom=0;
    for(int i=0;i<81;++i)
            sudoku[i]=QString (" ");
    ui->grid_0->setText(sudoku[0]);
    ui->grid_1->setText(sudoku[1]);
    ui->grid_2->setText(sudoku[2]);
    ui->grid_3->setText(sudoku[3]);
    ui->grid_4->setText(sudoku[4]);
    ui->grid_5->setText(sudoku[5]);
    ui->grid_6->setText(sudoku[6]);
    ui->grid_7->setText(sudoku[7]);
    ui->grid_8->setText(sudoku[8]);
    ui->grid_9->setText(sudoku[9]);

    ui->grid_10->setText(sudoku[10]);
    ui->grid_11->setText(sudoku[11]);
    ui->grid_12->setText(sudoku[12]);
    ui->grid_13->setText(sudoku[13]);
    ui->grid_14->setText(sudoku[14]);
    ui->grid_15->setText(sudoku[15]);
    ui->grid_16->setText(sudoku[16]);
    ui->grid_17->setText(sudoku[17]);
    ui->grid_18->setText(sudoku[18]);
    ui->grid_19->setText(sudoku[19]);

    ui->grid_20->setText(sudoku[20]);
    ui->grid_21->setText(sudoku[21]);
    ui->grid_22->setText(sudoku[22]);
    ui->grid_23->setText(sudoku[23]);
    ui->grid_24->setText(sudoku[24]);
    ui->grid_25->setText(sudoku[25]);
    ui->grid_26->setText(sudoku[26]);
    ui->grid_27->setText(sudoku[27]);
    ui->grid_28->setText(sudoku[28]);
    ui->grid_29->setText(sudoku[29]);

    ui->grid_30->setText(sudoku[30]);
    ui->grid_31->setText(sudoku[31]);
    ui->grid_32->setText(sudoku[32]);
    ui->grid_33->setText(sudoku[33]);
    ui->grid_34->setText(sudoku[34]);
    ui->grid_35->setText(sudoku[35]);
    ui->grid_36->setText(sudoku[36]);
    ui->grid_37->setText(sudoku[37]);
    ui->grid_38->setText(sudoku[38]);
    ui->grid_39->setText(sudoku[39]);

    ui->grid_40->setText(sudoku[40]);
    ui->grid_41->setText(sudoku[41]);
    ui->grid_42->setText(sudoku[42]);
    ui->grid_43->setText(sudoku[43]);
    ui->grid_44->setText(sudoku[44]);
    ui->grid_45->setText(sudoku[45]);
    ui->grid_46->setText(sudoku[46]);
    ui->grid_47->setText(sudoku[47]);
    ui->grid_48->setText(sudoku[48]);
    ui->grid_49->setText(sudoku[49]);

    ui->grid_50->setText(sudoku[50]);
    ui->grid_51->setText(sudoku[51]);
    ui->grid_52->setText(sudoku[52]);
    ui->grid_53->setText(sudoku[53]);
    ui->grid_54->setText(sudoku[54]);
    ui->grid_55->setText(sudoku[55]);
    ui->grid_56->setText(sudoku[56]);
    ui->grid_57->setText(sudoku[57]);
    ui->grid_58->setText(sudoku[58]);
    ui->grid_59->setText(sudoku[59]);

    ui->grid_60->setText(sudoku[60]);
    ui->grid_61->setText(sudoku[61]);
    ui->grid_62->setText(sudoku[62]);
    ui->grid_63->setText(sudoku[63]);
    ui->grid_64->setText(sudoku[64]);
    ui->grid_65->setText(sudoku[65]);
    ui->grid_66->setText(sudoku[66]);
    ui->grid_67->setText(sudoku[67]);
    ui->grid_68->setText(sudoku[68]);
    ui->grid_69->setText(sudoku[69]);

    ui->grid_70->setText(sudoku[70]);
    ui->grid_71->setText(sudoku[71]);
    ui->grid_72->setText(sudoku[72]);
    ui->grid_73->setText(sudoku[73]);
    ui->grid_74->setText(sudoku[74]);
    ui->grid_75->setText(sudoku[75]);
    ui->grid_76->setText(sudoku[76]);
    ui->grid_77->setText(sudoku[77]);
    ui->grid_78->setText(sudoku[78]);
    ui->grid_79->setText(sudoku[79]);

    ui->grid_80->setText(sudoku[80]);
}
