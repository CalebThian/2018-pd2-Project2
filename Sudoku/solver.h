#ifndef SOLVER_H
#define SOLVER_H
#include "reader.h"
#include "dlxdata.h"
#include <vector>
#include <assert.h>

class Solver
{
public:
    Solver();
    void elementary();
    void addAns1(int abc);
    void addAns2(/*int destination[]*/std::vector <int> destination, /*int ansAdded*/std::vector <bool> ansAdded);
    bool leter();
    int discriminant();
    int tryrep();
    void producer();
    void tuans();
    int getans1();
    void getans2();
    void readerread();
    void readertrans();
    //void logger();
private:
Reader Sivial;
/*bool vanscom;*/std::vector <bool> vanscom;
std::vector </*bool*/std::vector <bool> > vansset;
std::vector <int> vcutindex;
/*bool cvanscom;*/std::vector <bool> cvanscom;
/*int cvtotal;*/std::vector <int> cvtotal;
int ans[81];
int ndis;
int dis;
int qans;
int count;
//int noCol;
//DLXdata dlxarr[10000];
};

#endif // SOLVER_H
