#ifndef DLXDATA_H
#define DLXDATA_H


class DLXdata
{
public:
    DLXdata();
    DLXdata(int a);
    int center();
    int right();
    int left();
    int up();
    int down();
    int rows();
    int cols();
//private:
    int centerNo;
    int rightNo;
    int leftNo;
    int upNo;
    int downNo;
    int rowsNo;
    int colsNo;

};

#endif // DLXDATA_H
