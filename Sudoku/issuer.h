#ifndef ISSUER_H
#define ISSUER_H
#include <iostream>
#include <vector>
#include <cstdlib>
#include <ctime>

class Issuer
{
public:
    Issuer();
    void genElement();
    void sudokuGen();


private:
    std::vector <int> squaIndex;
    std::vector <bool> element;
    std::vector <std::vector<bool> > quesset;
    std::vector <bool> questotal;
    int ques[81];
};

#endif // ISSUER_H
